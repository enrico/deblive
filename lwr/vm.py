# live-wrapper - Wrapper for vmdebootstrap for creating live images
# (C) Iain R. Learmonth 2015 <irl@debian.org>
# See COPYING for terms of usage, modification and redistribution.
#
# lwr/vm.py - vmdebootstrap helpers

"""
The lwr.vm module provides helpers for calling vmdebootstrap as part of the
image creation process.

Directory listing of /live/
filesystem.packages
filesystem.packages-remove
filesystem.squashfs
initrd.img
vmlinuz

.. note::
    This module requires that the vmdebootstrap modules be available in the
    Python path.
"""

import os
import contextlib
import tempfile
from vmdebootstrap.codenames import Codenames
from .chroot import Chroot
from .debootstrap import Debootstrap


@contextlib.contextmanager
def umask(mask):
    old_mask = os.umask(mask)
    yield
    os.umask(old_mask)


def run_vmdebootstrap(distribution, architecture, mirror, cdroot, customise, apt_mirror):
    # TODO: "mirror": mirror,
    # TODO: "hostname": "debian",

    distro = Codenames(distribution)
    include = sorted(set((
        distro.kernel_package(architecture),
        "sudo", "acpid",
        "python",  # for ansible
    )))

    with tempfile.TemporaryDirectory() as rootdir:
        with umask(0o022):
            chroot = Chroot(rootdir)
            debootstrap = Debootstrap(
                    chroot,
                    distribution=distribution,
                    arch=architecture,
                    include=include,
            )
            debootstrap.run()
            chroot.remove_udev_persistent_rules()
            chroot.run_ansible("chroot.yaml", mirror=apt_mirror, distribution=distribution)
            chroot.apt_clean_cache()
            chroot.update_initramfs()
            chroot.run_mksquashfs(os.path.join(cdroot, 'live'))


def detect_kernels(cdroot):
    versions = []
    filenames = os.listdir(os.path.join(cdroot, "live"))
    for filename in filenames:
        if filename[0:8] == "vmlinuz-":
            versions.append(filename[8:])
    return versions
