import tempfile
import os
import subprocess
import shutil
import shlex
import logging


class Chroot:
    # TODO: use shutil.which when py3
    ansible_playbook = shutil.which("ansible-playbook")

    # TODO: default to xz
    squashfs_compression = "lzo"

    # TODO: replace with shutil.which in py3
    mksquashfs = shutil.which("mksquashfs")

    def __init__(self, path):
        self.path = path
        self.log = logging.getLogger("chroot")

    def run_ansible(self, playbook, **kw_vars):
        self.log.info("Customizing %s with %s", self.path, playbook)

        with tempfile.NamedTemporaryFile(suffix=".ini", mode="wt") as ansible_inventory:
            print("[live]", file=ansible_inventory)
            print("{} ansible_connection=chroot {}".format(
                    self.path,
                    " ".join("{}={}".format(k, v) for k, v in kw_vars.items())),
                  file=ansible_inventory)
            ansible_inventory.flush()
            with tempfile.NamedTemporaryFile(suffix=".cfg", mode="wt") as ansible_cfg:
                print("[defaults]", file=ansible_cfg)
                print("nocows = 1", file=ansible_cfg)
                print("inventory = {}".format(ansible_inventory.name), file=ansible_cfg)
                ansible_cfg.flush()

                env = dict(os.environ)
                env["ANSIBLE_CONFIG"] = ansible_cfg.name
                subprocess.check_call([self.ansible_playbook, playbook], env=env)

    def run_mksquashfs(self, dest):
        if not os.path.exists(dest):
            os.makedirs(dest)
        suffixed = os.path.join(dest, "filesystem.squashfs")
        if os.path.exists(suffixed):
            os.unlink(suffixed)

        with tempfile.NamedTemporaryFile(mode="wt") as fd:
            print("/proc", file=fd)
            print("/dev", file=fd)
            print("/sys", file=fd)
            print("/run", file=fd)

            self.log.info("Running mksquashfs on %s", self.path)
            self.run_cmd(
                ['nice', self.mksquashfs, self.path, suffixed,
                 '-no-progress', '-comp', self.squashfs_compression,
                 '-e', fd.name])
            check_size = os.path.getsize(suffixed)
            self.log.debug("Created squashfs: %s (%d bytes)", suffixed, check_size)
            if check_size < (1024 * 1024):
                self.log.warning(
                    "%s appears to be too small: %s bytes",
                    suffixed, check_size)

            bootdir = os.path.join(self.path, 'boot')
            # copying the boot/* files
            self.log.debug("Copying boot files out of squashfs")
            self.copy_files(bootdir, dest)

    def remove_udev_persistent_rules(self):
        self.log.info('Removing udev persistent cd and net rules')
        for rule in '70-persistent-cd.rules', '70-persistent-net.rules':
            pathname = os.path.join(self.path, 'etc', 'udev', 'rules.d', rule)
            if os.path.exists(pathname):
                self.log.debug('Removing %s', pathname)
                os.remove(pathname)
            else:
                self.log.debug('Not removing non-existent %s', pathname)

    def update_initramfs(self):
        cmd = os.path.join('usr', 'sbin', 'update-initramfs')
        if os.path.exists(os.path.join(self.path, cmd)):
            self.log.info("Updating the initramfs")
            self.run_cmd(['chroot', self.path, cmd, '-u'])

    def copy_files(self, src, dest):
        """
        Copy all files in src to dest
        """
        for filename in os.listdir(src):
            src_path = os.path.join(src, filename)
            if os.path.isdir(src_path) or os.path.islink(src_path):
                continue
            shutil.copyfile(
                src_path,
                os.path.join(dest, filename))

    def run_cmd(self, args):
        self.log.debug("run: %s", " ".join(shlex.quote(x) for x in args))
        subprocess.check_call(args)

    def apt_clean_cache(self):
        self.run_cmd(['chroot', self.path, 'apt-get', 'clean'])
