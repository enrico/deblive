from __future__ import (print_function, absolute_import, unicode_literals, division)
import shutil
import shlex
from contextlib import contextmanager
import subprocess
import logging
import hashlib
import os


class Debootstrap:
    distribution = "stable"

    eatmydata = shutil.which("eatmydata")

    mirror = "http://deb.debian.org/debian/"

    include = []

    debootstrapopts = []

    cachedir = "."

    def __init__(self, chroot, **kw):
        self.log = logging.getLogger("debootstrap")
        self.chroot = chroot
        for k, v in kw.items():
            setattr(self, k, v)

    def run(self):
        self.log.info("Debootstrapping %s [%s]", self.distribution, self.arch)

        args = ['debootstrap', '--arch=' + self.arch]

        if self.include:
            args.append('--include=' + ','.join(sorted(self.include)))

        for opt in self.debootstrapopts:
            for part in opt.split(' '):
                args.append('--' + part)

        args += [self.distribution, self.chroot.path, self.mirror]
        args = [self.eatmydata] + args

        with self.cache() as hit:
            if not hit:
                self.log.debug("debootstrap arguments: %s", args)
                self.run_cmd(args)
                self.chroot.apt_clean_cache()

    @contextmanager
    def cache(self):
        if not self.cachedir:
            yield False
            return

        # Use sha to have something short and fast, we do not need secure
        sha = hashlib.sha1()
        sha.update(":".join([self.arch, self.distribution, self.mirror] + sorted(self.include) + sorted(self.debootstrapopts)).encode("utf8"))
        cache_id = sha.hexdigest()

        tarball_name = os.path.join(self.cachedir, "debootstrap-" + cache_id + ".tar.gz")
        if os.path.exists(tarball_name):
            self.log.info("%s found: reusing it", tarball_name)
            self.run_cmd(["tar", "-C", self.chroot.path, "-zxf", tarball_name])
            yield True
        else:
            self.log.info("%s not found: (re)creating it", tarball_name)
            yield False
            self.run_cmd(["tar", "-C", self.chroot.path, "-zcf", tarball_name, "."])

    def run_cmd(self, args):
        self.log.debug("run: %s", " ".join(shlex.quote(x) for x in args))
        subprocess.check_call(args)
